package control;

import java.io.*;
import java.io.IOException;

import view.*;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Account;
import model.AccountList;
import model.IO;
import model.Photo;
/**
 * launcher of the app
 * create and maintain basic fields on it
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public class Photos extends Application {
	/**
	 * the primary stage of the application
	 * scenes can alter within the stage
	 * scene size and location are adjusted according to the size of scene
	 */
	Stage primaryStage;
	/**
	 * all users are stored into this field
	 * the app only store the object of this field into inter-session data before the stopping of running of the app
	 */
	AccountList accounts;
	/**
	 * return all the account
	 * @return all the account
	 */
	public AccountList getAccounts() {
		return accounts;
	}
	/**
	 * set the collection of all the account of the app
	 * @param accounts the collection of all the account you want to set
	 */
	public void setAccounts(AccountList accounts) {
		this.accounts = accounts;
	}
	/**
	 * load and create all the data/data structure needed for primary scene and initial login scene
	 */
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
    	File tempFile = new File("data\\info.bin");
    	if(!tempFile.exists()) accounts = new AccountList();
    	else	this.accounts = IO.get();
		primaryStage.setOnCloseRequest(e -> {
			// System.out.println("close project");
			IO.write(accounts);
		});
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/LoginScene.fxml"));
			loader.load();
			LoginSceneController c = loader.getController();
			c.obtainPhotos(accounts);
			c.start(primaryStage);
			primaryStage.centerOnScreen();
			primaryStage.show();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * main method
	 * to start the app
	 * @param args VM arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
