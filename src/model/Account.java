package model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import java.io.Serializable;

/**
 * Account class
 * @author Zhonghao Wu Thomas Cooke
 * hierarchy of collection
 *Account:
 *	albums(Album)
 *		Album:
 *			photos(Photo)
 *				Photo:
 *					tags(Tag)
 *						Tag:
 *							values(String)
 *	
 */
public class Account implements Serializable{
	private Album favorite;
	/**
	 * username of the owner of the acount
	 */
	private String username;
	/**
	 * all the albums the user have
	 */
	transient ObservableList<Album> albums;
	/**
	 * data structure for storage
	 */
	public ArrayList<Album> backup;
	/**
	 * toStore customized Tag name
	 */
	public ArrayList<Tag> customizedTagType;
	/**
	 * only constructor of account
	 * @param username of the owner of the account
	 */
	public Account(String username) {
		super();
		favorite = new Album("favorite");
		this.setUsername(username);
		this.albums = FXCollections.observableArrayList();
		this.backup = new ArrayList<>();
		customizedTagType = new ArrayList<>();
		updateFavorite();
	}
	
	/**
	 * obtain the username of the account
	 * @return return the username of owner of the account
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * set the the username of the account 
	 * @param username username of the username of the account
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 * get all the albums within the account
	 * @return collection of all the albums of the account
	 */
	public ObservableList<Album> getalbums(){
		return albums;
	}

	/**
	 * get all the names of albums within the account
	 * @return collection of all the names of albums of the account
	 */
	public ObservableList<String> getAlbumsNames(){
		ObservableList<String> ret = FXCollections.observableArrayList();
		for (Album album : albums){
			ret.add(album.getName());
		}
		return ret;
	}
	
	/**
	 * obtain all the photos in the account that satisfy the date requirement of arguments
	 * @param lo the lower bound of the date requirement
	 * @param hi the higher bound of the date requirement
	 * @return the collections of all the photos in the account that satisfy the date requirement of arguments
	 */
	public ObservableList<Photo> getPhotoWithinTimeSpan(Calendar lo, Calendar hi){
		ObservableList<Photo> ret = FXCollections.observableArrayList();
		for(Album album : albums) {
			for(Photo photo : album.photos) {
				if(photo.withinTimeSpan(lo,hi))	ret.add(photo);
			}
		}
		return ret;
	}
	
	/**
	 * obtain all the photos in the account that satisfy the tag requirement of arguments
	 * one tag only for single tag search
	 * @param tagname1 the tag each return value must contains
	 * @param value the tag value
	 * @return the collection of all the photos in the account that satisfy the tag requirement of arguments
	 */
	public ObservableList<Photo> getPhotoWithGivenReqSingle(String tagname1, String value){
		ObservableList<Photo> ret = FXCollections.observableArrayList();
		for(Album album : albums) {
			for(Photo photo : album.photos) {
				if(photo.containsTag(tagname1) && photo.getTag(tagname1).hasValue(value)) ret.add(photo);
			}
		}
		return ret;
	}
	
	/**
	 * obtain all the photos in the account that satisfy the tag requirement of arguments
	 * two tags only for these method
	 * @param tagname1 the first tag each return value must contains
	 * @param value1 the first return value
	 * @param tagname2 the second tag each return value must contains
	 * @param value2 the second return value
	 * @param logic combinational logic can either be and or or
	 * @return the collection of all the photos in the account that satisfy the tag requirement of arguments
	 */
	public ObservableList<Photo> getPhotoWithGivenReq(String tagname1, String value1, String tagname2, String value2, String logic){
		ObservableList<Photo> ret = FXCollections.observableArrayList();
		for(Album album : albums) {
			for(Photo photo : album.photos) {
				if(logic.equals("&&")) {
					if((photo.containsTag(tagname1) && photo.getTag(tagname1).hasValue(value1)) && ((photo.containsTag(tagname2) && photo.getTag(tagname2).hasValue(value2)))) ret.add(photo);
				}else if(logic.equals("||")){
					if((photo.containsTag(tagname1) && photo.getTag(tagname1).hasValue(value1)) || ((photo.containsTag(tagname2) && photo.getTag(tagname2).hasValue(value2)))) ret.add(photo);
				}else	return null;
			}
		}
		return ret;
	}
	
	/**
	 * obtain the album with required username of the owner 
	 * @param name username of the owner
	 * @return the album with required username of the owner 
	 */
	public Album getAlbum(String name) {
		for(Album album : albums) {
			if(album.name.equals(name))	return album;
		}
		return null; 
	}
	
	/**
	 * add one album into the collection of albums of the account
	 * @param a the album
	 */
	public void addAlbum(Album a) {
		albums.add(a);
		backup = new ArrayList(albums);
	}
	
	/**
	 * delete the album in the collection of the account
	 * @param album the album object
	 */
	public void deleteAlbum(Album album) {
		albums.remove(album);
		backup = new ArrayList(albums);
	}
	
	/**
	 * obtain an Album with given collection of photos and album name 
	 * @param photos collection of photos
	 * @param name name of new created album
	 * @return the wanted album
	 */
	public Album returnAnAlbum(ObservableList<Photo> photos, String name){
		return new Album(name, photos);
	}
	
	/**
	 * just for test
	 * print the tree of the account
	 */
	public void printAccount() {
		System.out.println(username);
		for(Album album : albums) {
			System.out.println(" -" + album.name);
			for(Photo photo : album.photos) {
				System.out.println("  -" + photo.name);
				for(Tag tag : photo.tags) {
					System.out.print("   -");
					System.out.println(tag);
				}
			}
		}
	}
	
	/**
	 * test if the account have album with given name
	 * @param albumname the name of the album
	 * @return true if and only if the account have album with given name
	 */
	public boolean contains(String albumname) {
		for(Album album : albums) {
			if(album.getName().equals(albumname))	return true;
		}
		return false;
	}
	
	/**
	 * get all the unique tags of all the photos in the account
	 * @return the collection of all the account which has album with given name
	 */
	public Set<String> allTags(){
		Set<String> set = new HashSet<>();
		for(Album current : getalbums()) {
			for(Photo photo : current.getPhotos()) {
				for(Tag tag : photo.tags) {

					set.add(tag.getName());
				}
			}
		}
		for(Tag tag : this.customizedTagType) {
			set.add(tag.getName());
		}
		return set;
	}
	/**
	 * get all the key-value pair 
	 * @return collection of these key_value pairs
	 */
	public Set<Key_Value_Pair> allKVP(){
		Set<Key_Value_Pair> set = new HashSet<>();
		for(Album current : getalbums()) {
			for(Photo photo : current.getPhotos()) {
				for(Tag tag : photo.tags) {
					for(String value : tag.getValues()) {
						set.add(new Key_Value_Pair(tag.getName(), value));
					}
				}
			}
		}
		return set;
	}
	/**
	 * override to string method
	 */
	public String toString() {
		return this.username;
	}
	/**
	 * update the favorite album of the account
	 */
	public void updateFavorite() {
		this.favorite = new Album("favorite");
		Album target = null;
		for(Album album : this.albums) {
			if(album.getName().equals("favorite")) target = album;
			for(Photo photo : album.getPhotos()) {
				if(photo.isFavorite()) favorite.addPhoto(photo);
			}
		}
		this.deleteAlbum(target);
		this.albums.add(favorite);
	}
}
