package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;

/**
 * a class for collect all the accounts
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public class AccountList implements Serializable{
	/**
	 * the data structure to collect all the accounts
	 */
	private transient ObservableList<Account> accounts;
	/**
	 * the only constructor
	 */
	public  ArrayList<Account> backup;
	public AccountList(){
		accounts = FXCollections.observableArrayList();
		
		Account stock = new Account("stock");
		Album stock_album = new Album("stock");
		Tools.buildinAdd(stock_album, "stock\\maxresdefault.jpg", "DP", "DPproblem");
		Tools.buildinAdd(stock_album, "stock\\IMG_1443(20200401-082842).JPG", "chongchongchong", "chong");
		Tools.buildinAdd(stock_album, "stock\\IMG_1541(20200413-014239).JPG", "hi", "cat_say_hi");
		Tools.buildinAdd(stock_album, "stock\\IMG_1359(20200321-234427).JPG", "tech company structure", "company_structure");
		Tools.buildinAdd(stock_album, "stock\\IMG_1321(20200324-025546).jpg", "NP", "np_problem");
		Tools.buildinAdd(stock_album, "stock\\IMG_1307(20200313-042005).jpg", "file life cycle", "lifeCycle");
		Tools.buildinAdd(stock_album, "stock\\maxresdefault.jpg", "DP", "DPproblem");
		stock.addAlbum(stock_album);
		int count = 1;
		for(Photo photo : stock_album.getPhotos()) {
			photo.setDate("2020-0" + count++ + "-24");
		}
		stock_album.updateDates();
		accounts.add(stock);
		backup = new ArrayList<>(accounts);
	}
	/**
	 * test if the collection contains the account with given name
	 * @param username the username
	 * @return true if the collection contains the account with given name
	 */
	public boolean contains(String username) {
		for(Account a : accounts) {
			if(a.getUsername().equals(username))	return true;
		}
		return false;
	}
	/**
	 * add existent account into the collection
	 * @param account existent account
	 */
	public void addExsistentAccount(Account account) {
		if(contains(account.getUsername())) {
			//give an error message
			return;
		}
		accounts.add(account);
		backup = new ArrayList<>(accounts);
	}
	/**
	 * create a new account with given name and store it into the collection
	 * @param username given username
	 */
	public void addNewAccount(String username) {
		if(contains(username)) {
			//give an error message
			return;
		}
		accounts.add(new Account(username));
		backup = new ArrayList<>(accounts);
	}
	/**
	 * remove the account with given username 
	 * @param username given username
	 */
	public void removeAccount(String username) {
		if(!contains(username)) {
			//give an error message
			return;
		}
		for(Iterator<Account> iterator = accounts.iterator(); iterator.hasNext(); ) {
			if(iterator.next().getUsername().equals(username)) {
				iterator.remove();
			}
		}
		backup = new ArrayList<>(accounts);
	}
	/**
	 * get account with given username
	 * @param username given username
	 * @return account with given username
	 */
	public Account getAccount(String username) {
		for(Account a : accounts) {
			if(a.getUsername().equals(username))	return a;
		}
		return null;
	}
	/**
	 * get the collection of all the albums
	 * @return the collection of all the albums
	 */
	public ObservableList<Account> getAccounts() {
		return accounts;
	}
	/**
	 * set the collection of all the accounts
	 * @param accounts the collection of all accounts
	 */
	public void setAccounts(ObservableList<Account> accounts) {
		this.accounts = accounts;
		backup = new ArrayList<>(accounts);
	}
	/**
	 * get accounts without updating backup
	 * @param accounts the collection of all accounts
	 */
	public void simplesetAccounts(ObservableList<Account> accounts) {
		this.accounts = accounts;
	}
	/**
	 * remove the account in the collection
	 * @param account account you need to remove
	 */
	public void removeAccount(Account account) {
		if(!contains(account.getUsername())) {
			//give an error message
			return;
		}
		accounts.remove(account);
		backup = new ArrayList<>(accounts);
	}
}
