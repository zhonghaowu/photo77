package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * an album class
 * store all the data an album need
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public class Album implements Serializable{
	/**
	 * collection of all the photos the album has
	 */
	transient ObservableList<Photo> photos;
	/**
	 * the name of the album
	 */
	String name;
	/**
	 * the number of photos in the album
	 */
	Integer number = 0;
	public  ArrayList<Photo> backup;
	
	public String getStart_string() {
		return start_string;
	}
	/**
	 * get the end date string
	 * @return end date string
	 */
	public String getEnd_string() {
		return end_string;
	}

	/**
	 * start date of the album
	 */
	Calendar start;
	/**
	 * the end date string
	 */
	String start_string;
	/**
	 * the start date string
	 */
	String end_string;
	/**
	 * get the start date string
	 * @return the start date string
	 */
	public Calendar getStart() {
		return start;
	}
	/**
	 * set the start date of the album
	 * @param start the start date of the album
	 */
	public void setStart(Calendar start) {
		this.start = start;
	}
	/**
	 * get the end date of the album
	 * @return the end date of the album
	 */
	public Calendar getEnd() {
		return end;
	}
	/**
	 * set the end date of the album
	 * @param end the end date of the album
	 */
	public void setEnd(Calendar end) {
		this.end = end;
	}
	/**
	 * latest date of the album
	 */
	Calendar end;
	/**
	 * get the number of photos
	 * @return the number of photos
	 */
	public Integer getNumber() {
		return number;
	}
	/**
	 * set the number of photos
	 * @param number the number of photos
	 */
	public void setNumber(Integer number) {
		this.number = number;
	}
	/**
	 * the constructor of the class
	 * need a string
	 * @param name the name of the album
	 */
	public Album(String name) {
		super();
		this.photos = FXCollections.observableArrayList();
		this.name = name;
	}
	/**
	 * the constructor of the class
	 * need a string
	 * need a collection of photos
	 * @param name the name of the album
	 * @param photos the collection of photos
	 */
	public Album(String name, ObservableList<Photo> photos) {
		super();
		this.photos = photos;
		this.name = name;
		updateDates();
		backup = new ArrayList<>(photos);
	}
	
	/**
	 * get all the photos in the album
	 * @return the collection of all the photos in the album
	 */
	public ObservableList<Photo> getPhotos() {
		return photos;
	}
	
	/**
	 * set the collection of photos in the album
	 * @param photos the collection of photos in the album
	 */
	public void setPhotos(ObservableList<Photo> photos) {
		this.photos = photos;
		updateDates();
		backup = new ArrayList<>(photos);
	}
	public void simplesetPhotos(ObservableList<Photo> photos) {
		this.photos = photos;
		simpleupdateDates();
	}
	/**
	 * obtain the name of the album
	 * @return the name of the album
	 */
	public String getName() {
		return name;
	}
	/**
	 * set the name of the albbum
	 * @param name the new name of the album
	 */
	public void setName(String name) {
		this.name = name;
		backup = new ArrayList<>(photos);
	}
	/**
	 * get latest date of all photos
	 * @return latest date of all photos
	 */
	public Calendar getLatest() {
		if(photos.isEmpty())	return null;
		Calendar latest = photos.get(0).date;
		Tools.setHourMinSeconZero(latest);
		for(Photo photo : photos) {
			Calendar another = photo.date;
			Tools.setHourMinSeconZero(another);
			if(latest.after(photo))	latest = another;
		}
		return latest;
	}
	/**
	 * get earliest date of all photos
	 * @return earliest date of all photos
	 */
	public Calendar getEarlist() {
		if(photos.isEmpty())	return null;
		Calendar ealist = photos.get(0).date;
		Tools.setHourMinSeconZero(ealist);
		for(Photo photo : photos) {
			Calendar another = photo.date;
			Tools.setHourMinSeconZero(another);
			if(ealist.before(photo))	ealist = another;
		}
		return ealist;
	}
	/**
	 * add one photo into the album
	 * @param photo the given photo
	 */
	public void addPhoto(Photo photo) {
		if(photos.contains(photo))	return;
		photos.add(photo);
		number = photos.size();
		updateDates();
		backup = new ArrayList<>(photos);
	}
	/**
	 * remove a photo 
	 * @param photo object
	 * @return true if the photo was successfully removed
	 */
	public boolean removePhoto(Photo photo) {
		if(!photos.contains(photo))	return false;
		photos.remove(photo);
		number = photos.size();
		updateDates();
		backup = new ArrayList<>(photos);
		return true;
	}
	/**
	 * just for test
	 * print information of the album and information of all the photos within it
	 */
	public void printAlbum() {
		System.out.println(" -" + this.name);
		for(Photo photo : this.photos) {
			System.out.println("  -" + photo.name);
			for(Tag tag : photo.tags) {
				System.out.print("   -");
				System.out.println(tag);
			}
		}
	}
	/**
	 * update the dates
	 */
	public void updateDates() {
		updateStart();
		updateEnd();
		backup = new ArrayList<>(photos);
	}
	public void simpleupdateDates() {
		updateStart();
		updateEnd();
	}
	
	/**
	 * update the earliest date field
	 */
	public void updateStart() {
		if(photos.size() == 0) {
			this.start = null;
			return;
		}
		Calendar start = photos.get(0).date;
		for(Photo photo : photos) {
			if(photo.date.before(start)) {
				start = photo.date;
			}
		}
		this.start = start;
		SimpleDateFormat  formmat1 = new SimpleDateFormat("yyyy-MM-dd");
		this.start_string = formmat1.format(start.getTime());
	}
	/**
	 * update the latest date field
	 */
	public void updateEnd() {
		if(photos.size() == 0) {
			this.end = null;
			return;
		}
		Calendar end = photos.get(0).date;
		for(Photo photo : photos) {
			if(photo.date.after(end)) {
				end = photo.date;
			}
		}
		this.end = end;
		SimpleDateFormat  formmat1 = new SimpleDateFormat("yyyy-MM-dd");
		this.end_string = formmat1.format(end.getTime());
	}
}
