package model;
import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class IO {
	/**
	 * all the account's albums fields are filled
	 * all albums are fully filled
	 * @return the collection of all accounts
	 */
	public static AccountList get(){
        try {
			FileInputStream file = new FileInputStream("data\\info.bin");
			try {
				ObjectInputStream ois = new ObjectInputStream(file);
				try {
					AccountList ret = (AccountList) ois.readObject();
					ArrayList<Account> temp = ret.backup;
					if(temp == null)	return null;
					ObservableList<Account> copy = FXCollections.observableArrayList();
					ret.simplesetAccounts(copy);
					for(Account account : temp) {
						//copy account
						ArrayList<Album> temp2 = account.backup;
						if(temp2 == null)	continue;
						copy.add(account);
						account.albums = FXCollections.observableArrayList();
						for(Album album : temp2) {
							//copy album
							ArrayList<Photo> temp3 = album.backup;
							if(temp3 == null)	continue;
							account.albums.add(album);
							album.simplesetPhotos(FXCollections.observableArrayList());
							for(Photo photo : temp3) {
								//copy photo
								ArrayList<Tag> temp4 = photo.backup;
								if(temp4 == null)	continue;
								album.getPhotos().add(photo);
								photo.simplesetTags(FXCollections.observableArrayList());
								for(Tag tag : temp4) {
									//copy tag
									photo.simpleaddTag(tag);
									tag.simlpesetValues(FXCollections.observableArrayList(tag.backup));
								}
							}
						}
					}
					for(Account account : ret.getAccounts()) {
						account.updateFavorite();
					}
					ois.close();
					return ret;
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        return null;
	}
	/**
	 * write all the date into data/info.bin
	 * @param all the collection of all accounts
	 */
	public static void write(AccountList all) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("data\\info.bin"));
			oos.writeObject(all);
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
