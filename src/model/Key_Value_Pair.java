package model;

import java.io.Serializable;
import java.util.Collections;

import javafx.collections.ObservableList;

public class Key_Value_Pair implements Serializable{
	/**
	 * key of the pair
	 */
	public String key;
	/**
	 * value of the pair
	 */
	public String value;
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if(obj == null || !(obj instanceof Key_Value_Pair)) {return false;}
		Key_Value_Pair kvp2 = (Key_Value_Pair) obj;
		return (this.key.equals(kvp2.key)) && (this.value.equals(kvp2.value));
	}
	
    @Override
    public int hashCode() 
    { 
    	String x = key + value;
    	int ret = 0;
    	for(int i = 0; i < x.length(); i++) {
    		ret = (ret + x.charAt(i)) % 10000;
    	}
    	return ret;
    }
    /**
     * constructor for the pair
     * @param key the key of the pair
     * @param value the value of the pair
     */
	public Key_Value_Pair(String key, String value) {
		super();
		this.key = key;
		this.value = value;
	} 
	/**
	 * rewrite the toString method
	 */
	public String toString() {
		return this.key + "-" + this.value;
	}
}
