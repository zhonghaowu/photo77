package model;

import java.io.*;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * this class is defined as photo data structure
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public class Photo implements Serializable{
	/**
	 * url of the photo
	 */
	String url;
	/**
	 * collection for storing all the tags the photo has
	 */
	transient ObservableList<Tag> tags;
	/**
	 * caption of the photo
	 */
	String caption;
	/**
	 * true if the photo is favorite
	 */
	boolean favorite;
	/**
	 * name of the photo
	 */
	String name;
	/**
	 * date of the photo
	 */
	Calendar date;
	
	public ArrayList<Tag> backup;

	/**
	 * constructor for the photo
	 * @param url url of the photo
	 * @param caption caption of the photo
	 * @param favorite true if the photo is favorite
	 * @param name name of the photo
	 */
	public Photo(String url, String caption, boolean favorite, String name) {
		super();
		this.url = url;
		this.tags = FXCollections.observableArrayList();
		this.caption = caption;
		this.favorite = favorite;
		this.name = name;
		this.date = Calendar.getInstance();
		this.createTagIfKeyNotExist("Location", true);
		this.createTagIfKeyNotExist("Person", false);
		date.set(Calendar.MILLISECOND,0);
		backup = new ArrayList<>(tags);
	}
	/**
	 * obtain the url of the photo
	 * @return obtain the url of the photo
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * set the url of the photo
	 * @param url the input url
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * get the collection of all the tags the photo has
	 * @return collection of all the tags the photo has
	 */
	public ObservableList<Tag> getTags() {
		return tags;
	}
	/**
	 * set the collection of tags of the photo
	 * @param tags collection of tags of the photo
	 */
	public void setTags(ObservableList<Tag> tags) {
		this.tags = tags;
	}
	public void simplesetTags(ObservableList<Tag> tags) {
		this.tags = tags;
	}
	/**
	 * get the caption of the photo
	 * @return the caption of the photo
	 */
	public String getCaption() {
		return caption;
	}
	/**
	 * set the caption of the photo
	 * @param caption given new caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
		backup = new ArrayList<>(tags);
	}
	/**
	 * check if the photo is favorite
	 * @return true if the photo is favorite
	 */
	public boolean isFavorite() {
		return favorite;
	}
	/**
	 * set if the photo is favorite
	 * @param favorite truth value of the fact that if the photo is favorite
	 */
	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
		backup = new ArrayList<>(tags);
	}
	/**
	 * obtain name of the photo
	 * @return name of the photo
	 */
	public String getName() {
		return name;
	}
	/**
	 * set the photo name with given name 
	 * @param name given new name
	 */
	public void setName(String name) {
		this.name = name;
		backup = new ArrayList<>(tags);
	}
	/**
	 * obtain the date of the photo
	 * @return the date of the photo
	 */
	public Calendar getDate() {
		return date;
	}
	/**
	 * set the date of the photo
	 * @param date given new date
	 */
	public void setDate(Calendar date) {
		this.date = date;
		backup = new ArrayList<>(tags);
	}
	/**
	 * check if the date of the photo is between givem lower bound and higher bound
	 * @param lo lower bound date
	 * @param hi higher bound date
	 * @return the fact
	 */
	public boolean withinTimeSpan(Calendar lo, Calendar hi) {
		Tools.setHourMinSeconZero(this.date);
		Tools.setHourMinSeconZero(lo);
		Tools.setHourMinSeconZero(hi);

		hi.set(Calendar.MILLISECOND, 0);
		if(this.date.equals(lo) || this.date.equals(hi) || (hi.after(this.date) && lo.before(this.date))) {
			return true;
		}
		return false;
	}
	/**
	 * get the string of the date of the photo
	 * @return the string of the date of the photo
	 */
	public String getDateString() {
		SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy KK:mm a");
		return format.format(this.date.getTime());
	}
	/**
	 * check if the photo contain given key
	 * @param key given key
	 * @return true if the photo contain given key
	 */
	public boolean containsTag(String key) {
		boolean flag = false;
		for(Tag tag : tags) {
			if(tag.getName().equals(key)) {
				flag = true;
				break;
			}
		}
		return flag;
	}
	/**
	 * get the tag with given key
	 * @param key given key
	 * @return the tag with given key
	 */
	public Tag getTag(String key) {
		Tag target = null;
		for(Tag tag : tags) {
			if(tag.getName().equals(key)) {
				target = tag;
				break;
			}
		}
		return target;
	}
	/**
	 * create tag if the key doesn't exist
	 * @param key given key
	 * @param isSingleValued if the tag is singlevalued
	 */
	public void createTagIfKeyNotExist(String key, boolean isSingleValued) {
		if(!containsTag(key))	tags.add(new Tag(key, isSingleValued));
		backup = new ArrayList<>(tags);
	}
	/**
	 * add a new tag with value
	 * @param key given key
	 * @param value given value
	 * @return true if the key is already exist
	 */
	public boolean addTag(String key, String value) {
		//tag: true if the key is already exist
		Tag target = null;
		for(Tag tag : tags) {
			if(tag.getName().equals(key)) {
				target = tag;
				break;
			}
		}
		backup = new ArrayList<>(tags);
		return target.addValue(value);
	}
	/**
	 * delete tag with given value
	 * @param key the tag key
	 * @param value the tag value
	 * @return if the tag successfully deleted
	 */
	public boolean deleteTag(String key, String value) {
		if(!containsTag(key))	return false;
		this.getTag(key).removeValue(value);
		backup = new ArrayList<>(tags);
		return true;
	}
	
	/**
	 * just for test
	 * @param args VM argument
	 */
	/*public static void main(String[] args) {
		Photo photo = new Photo("", "", false, "testcase0");
		photo.createTagIfKeyNotExist("person", false);
		photo.addTag("person", "nmsl");
		photo.addTag("person", "?");
		photo.addTag("person", "dasknl");
		photo.createTagIfKeyNotExist("location", false);
//		photo.deleteTag("person");
//		photo.deleteTag("location");
//		photo.deleteTag("person");
		// System.out.println(photo.tags.size());
	}*/
	
	/**
	 * set the date of the photo
	 * @param str the inputed string
	 */
	public void setDate(String str) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		try {
			this.date.setTime(sdf.parse(str));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}// all done
		backup = new ArrayList<>(tags);
	}
	/**
	 * test if the photo contains given key_value pair
	 * @param kvp given key_value pair
	 * @return true if it contains given key_value pair
	 */
	public boolean containsKVP(Key_Value_Pair kvp) {
		for(Tag tag : this.tags) {
			if(!tag.getName().equals(kvp.key))	continue;
			for(String value : tag.getValues()) {
				if(value.equals(kvp.value))	return true;
			}
		}
		return false;
	}
	public void addTag(Tag tag) {
		// TODO Auto-generated method stub
		this.tags.add(tag);
		backup = new ArrayList<>(tags);
	}
	public void simpleaddTag(Tag tag) {
		// TODO Auto-generated method stub
		this.tags.add(tag);
	}
}	
