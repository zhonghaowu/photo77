package model;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * create a instance of the class Tag with tag name and if it can be multiple valued;
 * then use method addValue() to add value of the instance,
 * if cannot add value, false will return
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public class Tag implements Serializable{
	/**
	 * collection of tag values
	 */
	private transient ObservableList<String> values;
	/**
	 * name of the tag
	 */
	private String name;
	/**
	 * if the tag is single valued
	 */
	private boolean isSingleValued;
	
	public  ArrayList<String> backup;
	/**
	 * constructor of tag class
	 * @param name of the tag
	 * @param isSingleValued if the tag is single valued
	 */
	public Tag(String name, boolean isSingleValued) {
		super();
		this.values = FXCollections.observableArrayList();
		this.name = name;
		this.isSingleValued = isSingleValued;
		backup = new ArrayList<>(values);
	}
	/**
	 * get the collection of all the values the tag has
	 * @return the return value
	 */
	public ObservableList<String> getValues() {
		return values;
	}
	/**
	 * add an value into this tag
	 * @param value given value
	 * @return if the value has successfuly added
	 */
	public boolean addValue(String value) {
		if(isSingleValued && !values.isEmpty())	return false;
		if(values.contains(value))	return false;
		this.values.add(value);
		backup = new ArrayList<>(values);
		return true;
	}
	/**
	 * remove a value of the tag
	 * @param value value name 
	 * @return if it has been removed
	 */
	public boolean removeValue(String value) {
		boolean res =  values.remove(value);
		backup = new ArrayList<>(values);
		return res;
	}
	/**
	 * get the name of the tag
	 * @return name of the tag
	 */
	public String getName() {
		return name;
	}
	/**
	 * set the name of the tag
	 * @param name new tag name
	 */
	public void setName(String name) {
		this.name = name;
		backup = new ArrayList<>(values);
	}
	/**
	 * check if the tag is single valued
	 * @return true if it is single valued
	 */
	public boolean isSingleValued() {
		return isSingleValued;
	}
	/**
	 * set if the tag is single valued
	 * @param isSingleValued if the tag is single valued
	 */
	public void setSingleValued(boolean isSingleValued) {
		this.isSingleValued = isSingleValued;
		backup = new ArrayList<>(values);
	}
	/**
	 * check if the tag has given value
	 * @param value given value
	 * @return true if the tag has given value
	 */
	public boolean hasValue(String value) {
		for(String thisValue : values) {
			if(value.equals(thisValue))	return true;
		}
		return false;
	}
	/**
	 * override toString method
	 */
	public String toString() {

		String str = name + ": ";
		if (this.getValues().isEmpty()) {
			str = "";
		} else {
			for(String value : values) {
				str = str + value;
				str += ", ";
			}
			str = str.substring(0, str.length() - 2);
		}

		return str;
	}
	
    @Override
    public int hashCode() 
    { 
    	String all = name;
    	Collections.sort(values);
    	for(String value : values) {
    		all += name;
    	}
    	int res = 0;
    	for(int i = 0; i < all.length(); i++) {
    		res = (res + i) % 100000000;
    	}
    	return res;
    } 
    
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if(obj == null || !(obj instanceof Tag)) {return false;}
    	String all = name;
    	Collections.sort(values);
    	for(String value : values) {
    		all += name;
    	}
    	Tag tag2 = (Tag)obj;
    	String all2 = tag2.name;
    	// System.out.println(all + "\n" + all2);
    	ObservableList<String> collection = tag2.values;
    	Collections.sort(collection);
    	for(String value : collection) {
    		all2 += name;
    	}
    	return all.equals(all2);
	}
	/**
	 * the the value of the tag
	 * @param observableArrayList backup information
	 */
	public void setValues(ObservableList<String> observableArrayList) {
		// TODO Auto-generated method stub
		this.values = observableArrayList;
		backup = new ArrayList<>(values);
	}
	/**
	 * set the value of the tag without updating backup
	 * @param observableArrayList backup information
	 */
	public void simlpesetValues(ObservableList<String> observableArrayList) {
		// TODO Auto-generated method stub
		this.values = observableArrayList;
	}
}
