package model;

import java.io.File;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

import javafx.stage.FileChooser;
/**
 * class for storing static tool
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public class Tools implements Serializable{
	/**
	 * set the hour, minute, second, millisecond to zero
	 * @param cal1 the calendar you want to set
	 */
	public static void setHourMinSeconZero(Calendar cal1) {
		cal1.set(Calendar.HOUR_OF_DAY, 0);
		cal1.set(Calendar.MINUTE, 0);
		cal1.set(Calendar.SECOND, 0);
		cal1.set(Calendar.MILLISECOND, 0);
	}
	/**
	 * copy a photo to another album
	 * @param photo the copy photo to another album
	 * @param another destination album
	 */
	public static void copyCopyTo(Photo photo, Album another) {
		another.addPhoto(photo);;
	}
	/**
	 * move a photo to another album
	 * @param original original album
	 * @param photo target photo
	 * @param another target album
	 * @return if the photo has successfullt copy to target album
	 */
	public static boolean moveCopyTo(Album original, Photo photo, Album another) {
		if(!original.photos.contains(photo))	return false;
		original.removePhoto(photo);

		another.addPhoto(photo);
		return true;
	}
	/**
	 * buildin method for adding an photo
	 * @param currentAlbum current album
	 * @param address album url
	 * @param caption caption of the photo
	 * @param name the name of the photo
	 */
	static void buildinAdd(Album currentAlbum, String address, String caption, String name) {
        File img = new File(address);
        String filepath = img.getAbsolutePath();
        File file = new File(filepath);
        URL url = null;
		try {
			url = file.toURI().toURL();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Photo newPhoto;
        newPhoto = new Photo(url.toString(), "", false, img.getName());
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(file.lastModified());
        newPhoto.setDate(cal);
        newPhoto.setName(name);
        newPhoto.setCaption(caption);
        currentAlbum.addPhoto(newPhoto);

	}
}
