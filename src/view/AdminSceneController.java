package view;

import java.io.IOException;

import control.Photos;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Account;
import model.AccountList;
import model.IO;

/**
 * administor scene controller
 * @author Zhonghao Wu Thomas Cooke
 *
 */
public class AdminSceneController {
	@FXML
	private Button logoutButton;
	@FXML
	private ListView<Account> users;
	@FXML
	private Button deleteButton;
	@FXML
	private Button createUserButton;
	@FXML
	private AnchorPane adminPane;
	/**
	 * all the accounts the app keeps
	 */
	AccountList accounts;
	/**
	 * load information for GUI
	 * @param primaryStage the main stage
	 */
	public void start(Stage primaryStage) {
		primaryStage.setTitle("photo manager");
		Scene scene = new Scene(adminPane,250,380);
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		primaryStage.centerOnScreen();
		ObservableList<Account> obs = accounts.getAccounts();
		users.setItems(obs);
		logoutButton.setOnAction(e -> backToLogin(primaryStage));
		deleteButton.setOnAction(e -> remove(primaryStage));
		createUserButton.setOnAction(e -> newUser());
		primaryStage.setOnCloseRequest(e -> {
			// System.out.println("close project");
			IO.write(accounts);
		});
	}
	/**
	 * go back to login scene
	 * @param primaryStage the main stage
	 */
	public void backToLogin(Stage primaryStage) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/LoginScene.fxml"));
		try {
			loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LoginSceneController c = loader.getController();
		c.obtainPhotos(accounts);
		c.start(primaryStage);
	}
	/**
	 * create a new user window for creating new user
	 */
	public void newUser() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/NewuserScene.fxml"));
		try {
			loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		NewuserSceneController c = loader.getController();
		c.obtainPhotos(accounts);
		c.display();
	}
	/**
	 * obtain all the collection of all accounts
	 * @param p the return accountlist
	 */
	public void obtainPhotos(AccountList p) {
		this.accounts = p;
	}
	/**
	 * remove chosen account
	 * @param primaryStage the main stage
	 */
	public void remove(Stage primaryStage) {
		Account account = users.getSelectionModel().getSelectedItem();
		if(account == null) {
			noChosenError(primaryStage);
			return;
		}
		accounts.getAccounts().remove(account);
		successRemoveReport(primaryStage, account.getUsername());
	}
	/**
	 * give an error window for no chosen album
	 * @param mainStage the main stage
	 */
	private void noChosenError(Stage mainStage) {                
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner(mainStage);
		alert.setTitle("Invalid account selection report");
		alert.setHeaderText("one Account must be chosen");
		alert.showAndWait();
	}
	/**
	 * give an information window for successfully removing an album
	 * @param mainStage the main stage
	 * @param username username of the account 
	 */
	private void successRemoveReport(Stage mainStage, String username) {                
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.initOwner(mainStage);
		alert.setTitle("remove success");
		alert.setHeaderText("the user \"" + username + "\" is successfuly removed.");
		alert.showAndWait();
	}
}
