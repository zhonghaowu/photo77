package view;

import java.io.IOException;
import java.util.ArrayList;

import control.Photos;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Account;
import model.AccountList;
import model.IO;

public class LoginSceneController {
	@FXML
	private Button newUserButton;
	@FXML
	private Button loginButton;
	@FXML
	private TextField usernameTextField;
	@FXML
	private AnchorPane loginPane;
	/**
	 * this field keeps all the inter-session information
	 */
	AccountList accounts;
	/**
	 * necessary for javafx
	 * do preparation for login scene
	 * and show it
	 * @param primaryStage the main stage
	 */
	public void start(Stage primaryStage) {
		primaryStage.setTitle("photo manager");
		Scene scene = new Scene(loginPane,320,200);
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		newUserButton.setOnAction(e -> dealNewUser(primaryStage));
		primaryStage.centerOnScreen();
		loginButton.setOnAction(e -> dealLogin(usernameTextField.getText(), primaryStage));
		primaryStage.setOnCloseRequest(e -> {
			// System.out.println("close project");
			IO.write(accounts);
		});
		this.accounts.backup = new ArrayList<Account>(accounts.getAccounts());
	}
	/**
	 * deal with login
	 * @param username inputed username
	 * @param primaryStage the main stage
	 */
	public void dealLogin(String username, Stage primaryStage) {
		if(username.equals("Admin")) {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/AdminScene.fxml"));
			try {
				loader.load();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			AdminSceneController c = loader.getController();
			c.obtainPhotos(accounts);
			c.start(primaryStage);			
		}else if(accounts.contains(username)) {
			// System.out.println("login success");
			//login
			Account account = accounts.getAccount(username);
			goToAlbumBrowseScene(primaryStage, account);
		}else {
			//give error message
		}
	}
	/**
	 * deal with new user process request
	 * @param primaryStage the main stage
	 */
	public void dealNewUser(Stage primaryStage) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/NewuserScene.fxml"));
		try {
			loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		NewuserSceneController c = loader.getController();
		c.obtainPhotos(accounts);
		c.start(primaryStage);	
	}
	/**
	 * load the accountList field 
	 * @param accounts accountList that need to load in field in this class
	 */
	public void obtainPhotos(AccountList accounts) {
		this.accounts = accounts;
	}
	/**
	 * do necessary work that need in transition to browse scene
	 * @param primaryStage the main stage
	 * @param account collection of all different account
	 */
	public void goToAlbumBrowseScene(Stage primaryStage, Account account) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/NonAdminMainScene.fxml"));
		try {
			loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		NonAdminMainSceneController c = loader.getController();
		c.obtainAccount(account);
		c.obtainPhotos(accounts);
		c.start(primaryStage);
	}
	/**
	 * load the accountList field 
	 * @param accounts accountList that need to load in field in this class
	 */
	public void obtainAccountList(AccountList accounts) {
		this.accounts = accounts;
	}
}
