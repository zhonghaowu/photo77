package view;

import java.io.IOException;

import com.sun.glass.ui.Window;

import control.Photos;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.AccountList;
import model.IO;

public class NewuserSceneController {
	@FXML
	private TextField textfield;
	@FXML
	private Button confirmButton;
	@FXML
	private Button cancelButton;
	@FXML
	private AnchorPane newUserPane;
	/**
	 * a mark for marking if the scene is independently exist
	 */
	private String state;
	/**
	 * a collection to store all account
	 */
	AccountList accounts;
	/**
	 * do necessary thing to load the scene
	 * @param primaryStage the main stage
	 */
	public void start(Stage primaryStage) {
		state = "independent";
		primaryStage.setTitle("user creating");
		Scene scene = new Scene(newUserPane,300,150);
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		confirmButton.setOnAction(e -> addNewUser(primaryStage));
		primaryStage.centerOnScreen();
		cancelButton.setOnAction(e -> back(primaryStage));
		primaryStage.setOnCloseRequest(e -> {
			// System.out.println("close project");
			IO.write(accounts);
		});
	}
	/**
	 * show the scene dependently
	 */
	public void display() {
		state = "dependent";
		Stage window = new Stage();
		window.setOnCloseRequest(e -> {
			IO.write(accounts);
		});
		window.initModality(Modality.APPLICATION_MODAL);
		window.setTitle("user creating");
		Scene scene = new Scene(newUserPane,300,150);
		window.setResizable(false);
		window.setScene(scene);
		confirmButton.setOnAction(e -> addNewUser(window));
		window.centerOnScreen();
		cancelButton.setOnAction(e -> back(window));
		window.show();
		window.setOnCloseRequest(e -> {
			// System.out.println("close project");
			IO.write(accounts);
		});
		
	}
	/**
	 * go back to the origiinal scene
	 * @param primaryStage the main scene
	 */
	public void back(Stage primaryStage) {
		if(state.equals("dependent"))	primaryStage.close();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/LoginScene.fxml"));
		try {
			loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LoginSceneController c = loader.getController();
		c.obtainPhotos(accounts);
		c.start(primaryStage);
	}
	/**
	 * handle the new user add request
	 * @param primaryStage the main scene
	 */
	public void addNewUser(Stage primaryStage) {
		String username = textfield.getText();
		if(username.equals("favorite") || username.equals("stock")) {
			undefinedOperation(primaryStage);
			return;
		}
		if(accounts.contains(username)) {
			existReport(primaryStage, username);
			//give error message
		}else {
			accounts.addNewAccount(username);
			sucessReport(primaryStage, username);
			back(primaryStage);
		}
		//add feedback(alert window)
	}
	/**
	 * obtain the collection of photos
	 * @param accounts the collection of all accounts
	 */
	public void obtainPhotos(AccountList accounts) {
		this.accounts = accounts;
	}
	/**
	 * if the user is successfully added
	 * @param mainStage the main scene
	 * @param username the inputed username
	 */
	private void sucessReport(Stage mainStage, String username) {                
	      Alert alert = new Alert(AlertType.INFORMATION);
	 	      alert.initOwner(mainStage);
	 	      alert.setTitle("User creating success");
	 	      alert.setHeaderText(
	 	           "The user \""+ username+ "\" is successfully created.");
	 	          alert.showAndWait();
	   }
	/**
	 * report the inputed username is already exist
	 * @param mainStage the main stage
	 * @param username inputed username
	 */
	private void existReport(Stage mainStage, String username) {                
	      Alert alert = new Alert(AlertType.ERROR);
	 	      alert.initOwner(mainStage);
	 	      alert.setTitle("User creating error");
	 	      alert.setHeaderText(
	 	           "The user name \""+ username+ "\" already exists.");
	 	          alert.showAndWait();
	   }
	/**
	 * report the operation user is doing is undefined
	 * @param mainStage the main stage
	 */
	private void undefinedOperation(Stage mainStage) {                
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner(mainStage);
		alert.setTitle("Invalid operation");
		alert.setHeaderText("this operation for album favorite is undefined");
		alert.showAndWait();
	}
}
