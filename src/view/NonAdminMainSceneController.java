package view;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Calendar;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Account;
import model.AccountList;
import model.Album;
import model.IO;
import model.Key_Value_Pair;
import model.Photo;
import model.Tag;

public class NonAdminMainSceneController {

    @FXML
    private AnchorPane mainAnchorPane;

    @FXML
    private TableView<Album> tableview;

    @FXML
    private TableColumn<Album,String> namecolumn;

    @FXML
    private TableColumn<Album,Integer> numbercolumn;

    @FXML
    private TableColumn<Album,String> earlistdatecolumn;

    @FXML
    private TableColumn<Album,String> latestphotocolumn;

    @FXML
    private Button createalbumButton;

    @FXML
    private Button deletealbumButton;

    @FXML
    private Button renameButton;

    @FXML
    private Button logoutButton;

    @FXML
    private DatePicker startDatePicker;

    @FXML
    private DatePicker endDatePicker;

    @FXML
    private Button searchByDateButton;

    @FXML
    private Button createByDateButton;

    @FXML
    private Button tagSearchButton;

    @FXML
    private Button createByTagButton;
    
    @FXML
    private Button browseButton;

    @FXML
    private ChoiceBox<Key_Value_Pair> tagBox1;

    @FXML
    private ChoiceBox<Key_Value_Pair> tagBox2;

    @FXML
    private RadioButton singleRadioButton;

    @FXML
    private RadioButton orRadioButton;

    @FXML
    private RadioButton andRadioButton;
    
    private AccountList accounts;
    
    private Account currentAccount;
    
    private ToggleGroup answer;
    
	/**
	 * load information for GUI
	 * @param primaryStage the main stage
	 */
	public void start(Stage primaryStage) {
		primaryStage.setTitle("photo manager");
		searchByDateButton.wrapTextProperty().setValue(true);
		createByDateButton.wrapTextProperty().setValue(true);
		tagSearchButton.wrapTextProperty().setValue(true);
		createByTagButton.wrapTextProperty().setValue(true);
		browseButton.wrapTextProperty().setValue(true);
		Scene scene = new Scene(mainAnchorPane,670, 327);
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		primaryStage.centerOnScreen();
		//setup table column
		namecolumn.setCellValueFactory(new PropertyValueFactory<Album,String>("name"));
		numbercolumn.setCellValueFactory(new PropertyValueFactory<Album,Integer>("number"));
		earlistdatecolumn.setCellValueFactory(new PropertyValueFactory<Album,String>("start_string"));
		latestphotocolumn.setCellValueFactory(new PropertyValueFactory<Album,String>("end_string"));
		tableview.setItems(currentAccount.getalbums());
		//set button handler
		createalbumButton.setOnAction(e -> dealNewAlubum(primaryStage));
		logoutButton.setOnAction(e -> backToLogin(primaryStage));
		deletealbumButton.setOnAction(
				e -> {
			if(tableview.getSelectionModel().getSelectedItem() == null) {
				noChosenError2(primaryStage);
				return;
			}
			if(tableview.getSelectionModel().getSelectedItem().getName().equals("favorite")) {
				undefinedOperation(primaryStage);
				return;
			}
			currentAccount.deleteAlbum(tableview.getSelectionModel().getSelectedItem());
		});
		renameButton.setOnAction(e -> handleRename(tableview.getSelectionModel().getSelectedItem(), primaryStage));
		searchByDateButton.setOnAction(e -> handleDateSearch(1, primaryStage));
		createByDateButton.setOnAction(e -> handleDateSearch(0, primaryStage));
		tagSearchButton.setOnAction(e -> handleTagSearch(1, primaryStage, ((RadioButton)this.answer.getSelectedToggle()).getText(), tagBox1, tagBox2));
		createByTagButton.setOnAction(e -> handleTagSearch(0, primaryStage, ((RadioButton)this.answer.getSelectedToggle()).getText(), tagBox1, tagBox2));
		browseButton.setOnAction(e -> goToPhotoBrowseScene(primaryStage, tableview.getSelectionModel().getSelectedItem()));
		//choicebox
		tagBox1.getItems().addAll(currentAccount.allKVP());
		tagBox1.getItems().add(null);
		tagBox2.getItems().addAll(currentAccount.allKVP());
		tagBox2.getItems().add(null);
		//toggle group
		answer = new ToggleGroup();
		//radio button
		singleRadioButton.setToggleGroup(answer);
		singleRadioButton.setSelected(true);
		andRadioButton.setToggleGroup(answer);
		orRadioButton.setToggleGroup(answer);
		primaryStage.setOnCloseRequest(e -> {
			// System.out.println("close project");
			IO.write(accounts);
		});
	}
	/**
	 * transfer scene to login scene
	 * @param primaryStage the main scene
	 */
	public void backToLogin(Stage primaryStage) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/LoginScene.fxml"));
		try {
			loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LoginSceneController c = loader.getController();
		c.obtainPhotos(accounts);
		c.obtainAccountList(accounts);
		c.start(primaryStage);
	}
	/**
	 * obtain the collection of all account
	 * @param accounts the collection of all account
	 */
	public void obtainPhotos(AccountList accounts) {
		this.accounts = accounts;
	}
	/**
	 * obtain the collection of all account
	 * @param accounts the collection of all accounts
	 */
	public void obtainAccount(Account accounts) {
		this.currentAccount = accounts;
	}
	/**
	 * obtain the collection of accounts
	 * @param accounts the collection of all accounts
	 */
	public void obtainAccountList(AccountList accounts) {
		this.accounts = accounts;
	}
	/**
	 * deal with the request for adding an new album
	 * @param primaryStage the main stage
	 */
	public void dealNewAlubum(Stage primaryStage) {
		TextInputDialog dialog = new TextInputDialog();
		dialog.initOwner(primaryStage);
		dialog.setTitle("New album");
		dialog.setHeaderText("input the name of new album");
		dialog.setContentText("blank username is invalid");
		Button okButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
		TextField inputField = dialog.getEditor();
		BooleanBinding isInvalid = Bindings.createBooleanBinding(() -> isInvalid(inputField.getText(), dialog), inputField.textProperty());
		okButton.setOnAction(e -> currentAccount.addAlbum(new Album(inputField.getText())));
		okButton.disableProperty().bind(isInvalid);
		dialog.showAndWait();
	}
/**
 * check if inputed text is valid
 * @param text inputed text
 * @param dialog text input dialog
 * @return true if the text inputed is valid
 */
	private boolean isInvalid(String text, TextInputDialog dialog) {
		// TODO Auto-generated method stub
		if(currentAccount.contains(text)) {
			dialog.setContentText("existent username");
			return true;
		}
		if(text.isEmpty()) {
			dialog.setContentText("blank username is invalid");
			return true;
		}
		dialog.setContentText("valid usernmae");
		return false;
	}
	/**
	 * handle with the request for renaming
	 * @param album the album user want to rename
	 * @param primaryStage the main stage
	 */
	public void handleRename(Album album, Stage primaryStage) {
		if(album == null) {
			noChosenError2(primaryStage);
			return;
		}
		if(album.getName().equals("favorite")) {
			undefinedOperation(primaryStage);
			return;
		}
		TextInputDialog dialog = new TextInputDialog();
		dialog.initOwner(primaryStage);
		dialog.setTitle("Rename album");
		dialog.setHeaderText("input new name of the album");
		dialog.setContentText("blank username is invalid");
		Button okButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
		TextField inputField = dialog.getEditor();
		BooleanBinding isInvalid = Bindings.createBooleanBinding(() -> isInvalid(inputField.getText(), dialog), inputField.textProperty());
		okButton.setOnAction(e -> {
			album.setName(inputField.getText());
			tableview.refresh();
		});
		okButton.disableProperty().bind(isInvalid);
		dialog.showAndWait();
	}
	
	/**
	 * mode == 0: get a new album by search res
	 * mode == 1: show search result
	 * @param primaryStage the main stage
	 * @param mode date search mode code
	 */
	public void handleDateSearch(int mode, Stage primaryStage) {
		LocalDate starttemp = startDatePicker.getValue();
		LocalDate endtemp = endDatePicker.getValue();
		if(starttemp == null || endtemp == null) {
			noChosenError(primaryStage);
			return;
		}
		Calendar start = Calendar.getInstance();
		start.clear();
	    start.set(starttemp.getYear(), starttemp.getMonthValue()-1, starttemp.getDayOfMonth());
	    Calendar end = Calendar.getInstance();
	    end.clear();
	    end.set(endtemp.getYear(), endtemp.getMonthValue()-1, endtemp.getDayOfMonth());
	    // System.out.println(starttemp);
	    // System.out.println(endtemp);
		Album album = new Album("search result");
		for(Album current : currentAccount.getalbums()) {
			for(Photo photo : current.getPhotos()) {
				if(photo.withinTimeSpan(start, end)) album.addPhoto(photo);
			}
		}
		if(start.after(end)) {
			inProperDate(primaryStage);
			return;
		}
		if(album.getPhotos().isEmpty()) {
			incompleteAlert(primaryStage);
			return;
		}
		if(mode == 1) {
			//show the album
			album.setName("search result " + starttemp.toString() + endtemp.toString());
			goToPhotoBrowseScene(primaryStage, album);
		}else if(mode == 0){
			album.setName("search result " + starttemp.toString() + endtemp.toString());
			currentAccount.addAlbum(album);
		}
	}
	/**
	 * method to handle tag search
	 * @param mode the mode of operation
	 * @param primaryStage the main scene
	 * @param logic logic of the combination
	 * @param cb1 choicebox1: tag
	 * @param cb2 choicebox2: tag
	 */
	public void handleTagSearch(int mode, Stage primaryStage, String logic, ChoiceBox<Key_Value_Pair> cb1, ChoiceBox<Key_Value_Pair> cb2) {
		for(Key_Value_Pair kvp : currentAccount.allKVP()) {
			// System.out.println(kvp);
		}
		logic = logic.trim();
		// System.out.println(logic);
		Album album = new Album("search result");
		//error check
		if(logic.equals("AND")) {
			if(cb1.getValue() == null || cb2.getValue() == null) {
				tagError(primaryStage, "two non-empty tags must be selected");
				return;
			}
		}else if(logic.equals("OR")) {
			if(cb1.getValue() == null || cb2.getValue() == null) {
				tagError(primaryStage, "two non-empty tags must be selected");
				return;
			}	
		}else {
			if(cb1.getValue() == null && cb2.getValue() == null) {
				tagError(primaryStage, "one non-empty tags must be selected");
				return;
			}
			if(cb1.getValue() != null && cb2.getValue() != null) {
				tagError(primaryStage, "only one non-empty tags must be selected");
				return;
			}			
		}
		if(cb1.getValue().equals(cb2.getValue())) {
			repeatTagError(primaryStage);
			return;
		}
		for(Album current : currentAccount.getalbums()) {
			for(Photo photo : current.getPhotos()) {
				if(logic.equals("AND")) {
					if(photo.containsKVP(cb1.getValue()) && photo.containsKVP(cb2.getValue()))	album.addPhoto(photo);
				}else if(logic.equals("OR")) {				
					if(photo.containsKVP(cb1.getValue()) || photo.containsKVP(cb2.getValue()))	album.addPhoto(photo);
				}else {
					if(cb2.getValue() == null && photo.containsKVP(cb1.getValue()))	album.addPhoto(photo);
					if(cb1.getValue() == null && photo.containsKVP(cb2.getValue()))	album.addPhoto(photo);
				}
			}
		}
		if(album.getPhotos().isEmpty()) {
			incompleteAlert(primaryStage);
			return;
		}
		if(mode == 1) {
			//show the album
			album.setName("search result " + cb1.getValue() + " " + logic 
					+ " " + cb2.getValue());
			goToPhotoBrowseScene(primaryStage, album);
		}else if(mode == 0){
			album.setName("search result " + cb1.getValue() + " " + logic 
					+ " " + cb2.getValue());
			currentAccount.addAlbum(album);
		}
	}
	/**
	 * give an incomplete stage alert
	 * @param mainStage the main scene
	 */
	private void incompleteAlert(Stage mainStage) {                
	      Alert alert = new Alert(AlertType.ERROR);
	 	      alert.initOwner(mainStage);
	 	      alert.setTitle("No result report");
	 	      alert.setHeaderText(
	 	           "No result satisfy search requirement");

	 	          alert.showAndWait();
	   }
	/**
	 * give an alert box for improper date chosen
	 * @param mainStage the main stage
	 */
	private void inProperDate(Stage mainStage) {                
	      Alert alert = new Alert(AlertType.ERROR);
	 	      alert.initOwner(mainStage);
	 	      alert.setTitle("Invalid date report");
	 	      alert.setHeaderText(
	 	           "Dates chosen are invalid");

	 	          alert.showAndWait();
	   }
	/**
	 * give an alert box for improper tag chosen
	 * @param mainStage the main stage
	 * @param string error information
	 */
	private void tagError(Stage mainStage, String string) {                
	      Alert alert = new Alert(AlertType.ERROR);
	 	      alert.initOwner(mainStage);
	 	      alert.setTitle("Invalid tag selection report");
	 	      alert.setHeaderText(string);

	 	          alert.showAndWait();
	   }
	/**
	 * give an alert for no two album chosen
	 * @param mainStage the main stage
	 */
	private void noChosenError(Stage mainStage) {                
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner(mainStage);
		alert.setTitle("Invalid date selection report");
		alert.setHeaderText("two date must be chosen");
		alert.showAndWait();
	}
	/**
	 * give an alert for no one album chosen
	 * @param mainStage the main stage
	 */
	private void noChosenError2(Stage mainStage) {                
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner(mainStage);
		alert.setTitle("Invalid date selection report");
		alert.setHeaderText("one album must be chosen");
		alert.showAndWait();
	}
	/**
	 * transition to photo browse scene
	 * @param mainStage the main stage
	 */
	private void repeatTagError(Stage mainStage) {                
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner(mainStage);
		alert.setTitle("Invalid tag selection report");
		alert.setHeaderText("two tag value pairs should be different.");
		alert.showAndWait();
	}
	/**
	 * give a report to say the operation user is doing is undefined
	 * @param mainStage
	 */
	private void undefinedOperation(Stage mainStage) {                
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner(mainStage);
		alert.setTitle("Invalid operation");
		alert.setHeaderText("this operation for album favorite is undefined");
		alert.showAndWait();
	}
	/**
	 * transition to photo browse scene
	 * @param primaryStage the primary stage
	 * @param album album user want to browse
	 */
	public void goToPhotoBrowseScene(Stage primaryStage, Album album) {
		if(album == null) {
			noChosenError2(primaryStage);
			return;
		}
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/PhotoBrowseScene.fxml"));
		try {
			loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PhotoBrowseSceneController c = loader.getController();
		c.obtainAccount(currentAccount);
		c.obtainPhotos(accounts);
		c.obtainAccountList(accounts);
		c.obtainAlbum(album);
		c.start(primaryStage);
	}

}
