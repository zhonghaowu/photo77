package view;


import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Pair;
import model.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class PhotoBrowseSceneController {

    @FXML
    private AnchorPane browsePane;

    @FXML
    private TextArea captionTextArea;

    @FXML
    private Button captionUpdateButton;

    @FXML
    private Button addButton;

    @FXML
    private Button removeButton;

    @FXML
    private ChoiceBox<String> destinationChoice;

    @FXML
    private RadioButton moveRadio;

    @FXML
    private RadioButton copyRadio;

    @FXML
    private Button moveCopyConfirmButton;

    @FXML
    private ListView<String> tagList;

    @FXML
    private Button addNewTagButton;

    @FXML
    private Button removeChosenTagButton;

    @FXML
    private TextField tagValueTextField;

    @FXML
    private ChoiceBox<String> tagKeyChoiceBox;

    @FXML
    private Button tagCustomizeButton;

    @FXML
    private ImageView imageView0;

    @FXML
    private ImageView imageView1;

    @FXML
    private ImageView imageView2;

    @FXML
    private ImageView imageView3;

    @FXML
    private ImageView imageView4;

    @FXML
    private ImageView imageView5;

    @FXML
    private ImageView imageViewSeparate;

    @FXML
    private Button prevPageButton;

    @FXML
    private Button nextPageButton;

    @FXML
    private Text tagText;

    @FXML
    private Text dataText;

    @FXML
    private Button logoutButton;

    @FXML
    private Text captionText;

    @FXML
    private Button slideShowButton;

    @FXML
    private RadioButton favoriteButton;

    @FXML
    private Button goToAlbums;

    @FXML
    private Text pageIDText;

    /**
     * a collection to store all account
     */
    private AccountList accounts;

    /**
     * the current account
     */
    private Account currentAccount;

    /**
     * the photo showing separately
     */
    private Photo photoSeparate;

    /**
     * the current album
     */
    private Album currentAlbum;

    /**
     * a toggle group for move or copy
     */
    private ToggleGroup moveOrCopy;

    /**
     * the ID of last photo
     */
    private int lastID;

    /**
     * the ID of last page
     */
    private int pageID;

    /**
     * the ID of current photo
     */
    private int currentID;

    /**
     * load information for GUI
     * @param primaryStage the main stage
     */
    public void start(Stage primaryStage) {
        update();
        //currentID = -1;
        Scene scene = new Scene(browsePane, 936, 511);
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.centerOnScreen();

        captionUpdateButton.setOnAction(e -> handleCaptionUpdate());
        addButton.setOnAction(e -> {
            try {
                handleAdd(primaryStage);
            } catch (MalformedURLException ex) {
                ex.printStackTrace();
            }
        });
        removeButton.setOnAction(e -> handleRemove(primaryStage));
        moveCopyConfirmButton.setOnAction(e -> handleCopyConfirm(primaryStage));
        addNewTagButton.setOnAction(e -> handleAddNewTag(primaryStage));
        removeChosenTagButton.setOnAction(e -> handleRemoveChosenTag());
        tagCustomizeButton.setOnAction(e -> handleTagCustomize(primaryStage));
        prevPageButton.setOnAction(e -> handlePrevPage());
        nextPageButton.setOnAction(e -> handleNextPage());
        logoutButton.setOnAction(e -> backToLogin(primaryStage));
        slideShowButton.setOnAction(e -> goToSlideShowScene(primaryStage));
        goToAlbums.setOnAction(e -> goToNonAdminMainScene(primaryStage));
        //toggle group
        moveOrCopy = new ToggleGroup();
        moveRadio.setToggleGroup(moveOrCopy);
        copyRadio.setToggleGroup(moveOrCopy);
        //radio button

        favoriteButton.setOnAction(e -> handleFavorite(primaryStage));
        //choice box



        destinationChoice.getItems().add(null);


        //image view


        imageView0.setPickOnBounds(true);
        imageView0.setOnMouseClicked((MouseEvent e) -> handleClick(pageID*6));
        imageView1.setPickOnBounds(true);
        imageView1.setOnMouseClicked((MouseEvent e) -> handleClick(pageID*6 + 1));
        imageView2.setPickOnBounds(true);
        imageView2.setOnMouseClicked((MouseEvent e) -> handleClick(pageID*6 + 2));
        imageView3.setPickOnBounds(true);
        imageView3.setOnMouseClicked((MouseEvent e) -> handleClick(pageID*6 + 3));
        imageView4.setPickOnBounds(true);
        imageView4.setOnMouseClicked((MouseEvent e) -> handleClick(pageID*6 + 4));
        imageView5.setPickOnBounds(true);
        imageView5.setOnMouseClicked((MouseEvent e) -> handleClick(pageID*6 + 5));
		primaryStage.setOnCloseRequest(e -> {
			// System.out.println("close project");
			IO.write(accounts);
		});
    }



    /**
     * update scene
     */
    public void update() {
        try {
            //image view
            imageViewSeparate.setImage(null);

            imageView0.setImage(null);
            imageView1.setImage(null);
            imageView2.setImage(null);
            imageView3.setImage(null);
            imageView4.setImage(null);
            imageView5.setImage(null);
            //text
            tagText.setWrappingWidth(250);
            dataText.setWrappingWidth(250);
            captionText.setWrappingWidth(250);
            tagText.setText("Tags: " );
            dataText.setText("Data: " );
            captionText.setText("Caption: ");
            captionTextArea.clear();

            favoriteButton.setSelected(false);
            tagList.getItems().clear();
            tagKeyChoiceBox.getItems().clear();
            String str = new String();


            lastID = currentAlbum.getPhotos().size() - 1;

            if (lastID < 0) {
                pageIDText.setText("- / -");
            } else {
                int cID = pageID + 1;
                int lID = lastID/6 + 1;
                pageIDText.setText(cID + " / " + lID);
            }

            imageViewSeparate.setImage(showImage(this.currentID));
            imageView0.setImage(showImage(pageID*6));
            imageView1.setImage(showImage(pageID*6 + 1));
            imageView2.setImage(showImage(pageID*6 + 2));
            imageView3.setImage(showImage(pageID*6 + 3));
            imageView4.setImage(showImage(pageID*6 + 4));
            imageView5.setImage(showImage(pageID*6 + 5));

            destinationChoice.getItems().setAll(currentAccount.getAlbumsNames());
            photoSeparate = currentAlbum.getPhotos().get(this.currentID);

            // favorite radio
            favoriteButton.setSelected(photoSeparate.isFavorite());
            //list view

            for (Tag tag : photoSeparate.getTags()) {
                for (String value : tag.getValues())
                tagList.getItems().add(tag.getName() + ": " + value);

            }



            currentAccount.allTags().forEach(item ->
                tagKeyChoiceBox.getItems().add(item));


            //if(!this.currentAlbum.getPhotos().isEmpty()) captionTextArea.setText(photoSeparate.getCaption());


            for (Tag tag : photoSeparate.getTags()) {
                str += tag.toString() + "  ";
            }
            tagText.setText("Tags: " + str);

            dataText.setText("Data: " + photoSeparate.getDateString());
            captionText.setText("Caption: " + photoSeparate.getCaption());
        } catch (Exception e) {
            tagText.setText("Tags: " );
            dataText.setText("Data: " );
            captionText.setText("Caption: ");
        }
    }

    /**
     * transfer scene to login scene
     * @param primaryStage the main scene
     */
    public void backToLogin(Stage primaryStage) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/LoginScene.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        LoginSceneController c = loader.getController();
        c.obtainPhotos(accounts);
        c.start(primaryStage);
    }

    /**
     * obtain the photos of all accounts
     * @param accounts the collection of all account
     */
    public void obtainPhotos(AccountList accounts) {
        this.accounts = accounts;
    }

    /**
     * obtain the collection of all account
     * @param accounts the collection of all account
     */
    public void obtainAccountList(AccountList accounts) {
        this.accounts = accounts;
    }
    /**
     * obtain the current account
     * @param account the current account
     */
    public void obtainAccount(Account account) {
        this.currentAccount = account;
    }

    /**
     * obtain the current album
     * @param album the current album
     */
    public void obtainAlbum(Album album) {
        this.currentAlbum = album;
    }

    /**
     * obtain the currentID
     * @param currentID the current ID
     */
    public void obtainCurrentID(int currentID) {
        this.currentID = currentID;
    }

    /**
     * hand click transfer
     * @param currentID the current ID
     */
    public void handleClick(int currentID) {
//        try {
//            imageViewSeparate.setImage(showImage(currentID));
//        } catch (Exception e) {
//            // do nothing
//        }
        if (currentID <= currentAlbum.getPhotos().size() - 1) {
            this.currentID = currentID;
            update();
        }

    }

    /**
     * show the image of current ID
     * @param currentID current ID
     * @return current image
     */
    public Image showImage(int currentID) {
        if (currentID < 0 || currentID > lastID) {
            return null;
        }
        Photo photo = currentAlbum.getPhotos().get(currentID);
        if (photo != null) {
            Image image = new Image(photo.getUrl());
            return image;

        }
        return null;
    }

    /**
     * show the favorite radio selected of the current photo and add it to the favorite album
     * @param primaryStage the main stage
     */
    public void handleFavorite(Stage primaryStage) {
    	if(currentAlbum.getName().equals("favorite")) {
    		this.undefinedOperation(primaryStage);
    		this.favoriteButton.selectedProperty().set(true);
    		return;
    	}
        try {
            photoSeparate.setFavorite(favoriteButton.isSelected());
            if(favoriteButton.isSelected()) {
            	currentAccount.getAlbum("favorite").addPhoto(photoSeparate);
            }else {
            	currentAccount.getAlbum("favorite").removePhoto(photoSeparate);
            }
            //favoriteButton.setSelected(photoSeparate.isFavorite());
        } catch (Exception e) {
            // do nothing
        }
        update();
    }

    /**
     * next page of current album
     */
    private void handleNextPage() {

        if (pageID*6 < 0 || pageID*6+5 >= lastID) {
            return;
        }
        pageID ++;
        this.currentID = pageID*6;
        update();
    }

    /**
     * prev page of current album
     */
    private void handlePrevPage() {

        if (pageID <= 0) {
            return;
        }
        pageID --;
        this.currentID = pageID*6;
        update();
    }

    /**
     * remove the chosen tag value
     */
    private void handleRemoveChosenTag() {
        try {
            String tag = tagList.getSelectionModel().getSelectedItem();
            String[] tagItems = tag.split(": ", 2);
            photoSeparate.deleteTag(tagItems[0], tagItems[1]);

        } catch (Exception e) {
            // do nothing
        }
        update();
    }

    /**
     * add new tag value
     */
    private void handleAddNewTag(Stage primaryStage) {
    	if(tagKeyChoiceBox.getValue() == null || tagValueTextField.getText().isEmpty()) {
    		this.emptyError(primaryStage);
    		return;
    	}

    	if(photoSeparate.containsKVP(new Key_Value_Pair(tagKeyChoiceBox.getValue(),tagValueTextField.getText()))) {
    		this.repeatError(primaryStage);
    		return;
    	}
        try {
            photoSeparate.addTag(tagKeyChoiceBox.getValue(),tagValueTextField.getText());

        } catch (Exception e) {
            // do nothing
        }
        update();

    }

    /**
     * customize tag key
     */
    private void handleTagCustomize(Stage primaryStage) {
        Optional<Pair<String, Boolean>> result = tagCustomizeDialog();

        result.ifPresent(tag -> {
        	for(Tag current : currentAccount.customizedTagType) {
        		if(current.getName().equals(result.get().getKey())) {
        			tagTypeExist(primaryStage);
        			return;
        		}
        	}
        	currentAccount.customizedTagType.add(new Tag(result.get().getKey(), result.get().getValue()));
            for (Album album : currentAccount.getalbums()) {
                for (Photo photo : album.getPhotos()) {
                    photo.createTagIfKeyNotExist(result.get().getKey(), result.get().getValue());
                }
            }

            // System.out.println("tag key :" + tag.getKey() + ", is single valued: " + tag.getValue());
        });
        update();
    }

    /**
     * customize tag key dialog
     */
    private Optional<Pair<String, Boolean>> tagCustomizeDialog() {
        // Create the custom dialog.
        Dialog<Pair<String, Boolean>> dialog = new Dialog<>();
        dialog.setTitle("customize tag key");
        dialog.setHeaderText("type in the tag key to add or remove");


        // Set the button types.
        ButtonType addButton = new ButtonType("Add", ButtonBar.ButtonData.OK_DONE);

        dialog.getDialogPane().getButtonTypes().addAll(addButton, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 10, 10, 10));

        TextField tagName = new TextField();
        tagName.setPromptText("Tag");

        CheckBox isSingleValued = new CheckBox();
        isSingleValued.setText("is single valued");

        grid.add(new Label("Tag: "), 0, 0);
        grid.add(tagName, 1, 0);
        grid.add(isSingleValued, 1, 1);

        Node addButtonNode = dialog.getDialogPane().lookupButton(addButton);
        ((Node) addButtonNode).setDisable(true);


        tagName.textProperty().addListener((observable, oldValue, newValue) -> {
            addButtonNode.setDisable(newValue.trim().isEmpty());
        });

        dialog.getDialogPane().setContent(grid);

        Platform.runLater(() -> tagName.requestFocus());

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == addButton) {
                return new Pair<>(tagName.getText(), isSingleValued.isSelected());
            }
            return null;
        });

        Optional<Pair<String, Boolean>> result = dialog.showAndWait();



        return result;
    }

    /**
     * copy or move based on moveOrCopy radio selected
     * @param primaryStage the main stage
     */
    private void handleCopyConfirm(Stage primaryStage) {
    	if(moveOrCopy.getSelectedToggle() == null) {
    		this.realUndefinedOperation(primaryStage);
    		return;
    	}
        try {
        	if(photoSeparate == null || currentAccount.getAlbum(destinationChoice.getValue()) == currentAlbum) {
        		this.realUndefinedOperation(primaryStage);
        		return;
        	}
        	if(currentAlbum.getName().equals("favorite") || destinationChoice.getValue().equals("favorite")) {
        		this.undefinedOperation(primaryStage);
        		return;
        	}
            if (moveOrCopy.getSelectedToggle().equals(moveRadio)) {
                Tools.moveCopyTo(currentAlbum, photoSeparate, currentAccount.getAlbum(destinationChoice.getValue()));
            } else if (moveOrCopy.getSelectedToggle().equals(copyRadio)) {
                Tools.copyCopyTo(photoSeparate, currentAccount.getAlbum(destinationChoice.getValue()));
            } else {
                // do nothing
            }
        }
        catch (Exception e) {
            // do nothing
        }
        this.currentID = pageID*6;;
        update();
    }

    /**
     * remove current photo
     * @param primaryStage the main stage
     */
    private void handleRemove(Stage primaryStage) {
    	if(currentAlbum.getName().equals("favorite")) {
    		undefinedOperation(primaryStage);
    		return;
    	}
        currentAlbum.removePhoto(photoSeparate);

        if (this.currentID != 0 && this.currentID % 6 == 0) {
            pageID--;
        }
        this.currentID = pageID*6;
        update();
    }

    /**
     * add photo
     * @param primaryStage the main stage
     */
    private void handleAdd(Stage primaryStage) throws MalformedURLException {
    	if(currentAlbum.getName().equals("favorite")) {
    		this.undefinedOperation(primaryStage);
    		return;
    	}
        FileChooser filechooser = new FileChooser();
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("Image Files",
                "*.png", "*.jpg", "*.jpeg", "*.gif");
        filechooser.getExtensionFilters().add(extFilterJPG);
        File img = filechooser.showOpenDialog(null);
        if (img == null) {
            return;
        } else {

            String filepath = img.getAbsolutePath();
            File file = new File(filepath);

            URL url = file.toURI().toURL();

            Photo newPhoto;


            newPhoto = new Photo(url.toString(), "", false, img.getName());


            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(file.lastModified());

            newPhoto.setDate(cal);

            currentAlbum.addPhoto(newPhoto);

        }
        this.currentID = currentAlbum.getPhotos().size()-1;
        if (this.currentID != 0 && this.currentID % 6 == 0) {
            pageID++;
        }
        update();
    }

    /**
     * update caption
     */
    private void handleCaptionUpdate() {
        try {
            photoSeparate.setCaption(captionTextArea.getText());
            //captionText.setText("Caption: " + photoSeparate.getCaption());
        } catch (Exception e) {
            // do nothing
        }
        update();
    }

    /**
     * go to slide show scene
     * @param primaryStage the main stage
     */
    public void goToSlideShowScene(Stage primaryStage) {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/SlideShowScene.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SlideShowSceneController c = loader.getController();

        c.obtainCurrentID(currentID);
        c.obtainAccountList(accounts);
        c.obtainAlbum(currentAlbum);
        c.obtainAccount(currentAccount);
        c.obtainPhotos(accounts);
        c.start(primaryStage);

    }

    /**
     * go to NonAdminMainScene
     * @param primaryStage the main stage
     */
    public void goToNonAdminMainScene(Stage primaryStage) {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/NonAdminMainScene.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        NonAdminMainSceneController c = loader.getController();
        c.obtainPhotos(accounts);
        c.obtainAccount(currentAccount);
        c.obtainAccountList(accounts);

        c.start(primaryStage);
    }


    /**
     * show alert of "Invalid operation"
     * @param mainStage the main stage
     */
	private void undefinedOperation(Stage mainStage) {                
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner(mainStage);
		alert.setTitle("Invalid operation");
		alert.setHeaderText("this operation for album favorite is illegal");
		alert.showAndWait();
	}
	/**
	 * give error information when tag value is empty
	 * @param mainStage the main stage
	 */
	private void emptyError(Stage mainStage) {                
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner(mainStage);
		alert.setTitle("Invalid operation");
		alert.setHeaderText("tag key or tag value should not be blank");
		alert.showAndWait();
	}
	
	private void repeatError(Stage mainStage) {                
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner(mainStage);
		alert.setTitle("Invalid operation");
		alert.setHeaderText("the key_value pair is already existed");
		alert.showAndWait();
	}
	/**
	 * say the operation is undefined
	 * @param mainStage the main stage
	 */
	private void realUndefinedOperation(Stage mainStage) {                
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner(mainStage);
		alert.setTitle("Invalid operation");
		alert.setHeaderText("the operation is illegal");
		alert.showAndWait();
	}
	private void tagTypeExist(Stage mainStage) {                
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner(mainStage);
		alert.setTitle("Invalid operation");
		alert.setHeaderText("the tag type already exist");
		alert.showAndWait();
	}

}
