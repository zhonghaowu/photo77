package view;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Account;
import model.AccountList;
import model.Album;
import model.IO;
import model.Photo;

public class SlideShowSceneController {




    @FXML
    private AnchorPane slideShowPane;

    @FXML
    private ImageView imageView;

    @FXML
    private Button prevButton;

    @FXML
    private Button nextButton;

    @FXML
    private Text imageID;

    @FXML
    private Button goToPhotosButton;

    @FXML
    private Button logoutButton;

    /**
     * a collection to store all account
     */
    private AccountList accounts;

    /**
     * the current account
     */
    private Account currentAccount;

    /**
     * the current album
     */
    private Album currentAlbum;

    /**
     * the ID of last photo
     */
    private int lastID;

    /**
     * the ID of current photo
     */
    private int currentID;

    /**
     * load information for GUI
     * @param primaryStage the main stage
     */
    public void start(Stage primaryStage) {
        try {
            lastID = currentAlbum.getPhotos().size() - 1;
            // currentID = 1;
            // set image view
            imageView.setImage(showImage(currentID));
        } catch (Exception e) {
            currentID = -1;
        }
        Scene scene = new Scene(slideShowPane,774,560);
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.centerOnScreen();
        int cID = currentID + 1;
        int lID = lastID + 1;
        imageID.setText(cID + " / " + lID);

        //set button handler
        prevButton.setOnAction(e -> prevImage());
        nextButton.setOnAction(e -> nextImage());
        logoutButton.setOnAction(e -> backToLogin(primaryStage));
        goToPhotosButton.setOnAction(e -> goToPhotoBrowseScene(primaryStage));
		primaryStage.setOnCloseRequest(e -> {
			// System.out.println("close project");
			IO.write(accounts);
		});
    }

    /**
     * show the image of current ID
     * @param currentID current ID
     * @return current image
     */
    public Image showImage(int currentID) {

        Photo photo = currentAlbum.getPhotos().get(currentID);
        if (photo != null) {

            return new Image(photo.getUrl());

        }
        return null;
    }

    /**
     * next image
     */
    public void nextImage() {

        if (currentID < 0 || currentID >= lastID) {
            return;
        }
        currentID ++;
        imageView.setImage(showImage(currentID));
        int cID = currentID + 1;
        int lID = lastID + 1;
        imageID.setText(cID + " / " + lID);
    }

    /**
     * prev image
     */
    public void prevImage() {

        if (currentID <= 0 || currentID > lastID) {
            return;
        }
        currentID --;
        imageView.setImage(showImage(currentID));
        int cID = currentID + 1;
        int lID = lastID + 1;
        imageID.setText(cID + " / " + lID);
    }

    /**
     * obtain the current album
     * @param album the current album
     */
    public void obtainAlbum(Album album) {
        currentAlbum = album;
        this.currentAlbum = album;
    }

    /**
     * obtain the currentID
     * @param currentID the current ID
     */
    public void obtainCurrentID(int currentID) {
        this.currentID = currentID;
    }

    /**
     * transfer scene to login scene
     * @param primaryStage the main scene
     */
    public void backToLogin(Stage primaryStage) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/LoginScene.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        LoginSceneController c = loader.getController();
        c.obtainPhotos(accounts);
        c.obtainAccountList(accounts);
        c.start(primaryStage);
    }

    /**
     * transition to photo browse scene
     * @param primaryStage the main stage

     */
    public void goToPhotoBrowseScene(Stage primaryStage) {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/PhotoBrowseScene.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        PhotoBrowseSceneController c = loader.getController();
        c.obtainAccount(currentAccount);		
        c.obtainAccountList(accounts);
        c.obtainPhotos(accounts);
        c.obtainAlbum(currentAlbum);
        c.obtainCurrentID(currentID);        
        c.start(primaryStage);
    }

    /**
     * obtain the collection of all account
     * @param accounts the collection of all account
     */
	public void obtainAccountList(AccountList accounts) {
		this.accounts = accounts;
	}
	 public void obtainAccount(Account currentAccount) {
		        this.currentAccount = currentAccount;
		    }
		
		    public void obtainPhotos(AccountList accounts) {
		        this.accounts = accounts;
		}
}
